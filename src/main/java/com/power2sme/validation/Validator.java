package com.power2sme.validation;

import java.util.regex.Pattern;

import com.power2sme.dto.Response;
import com.power2sme.dto.SendOtpRequest;
import com.power2sme.dto.Status;
import com.power2sme.dto.VerifyOtpRequest;

public class Validator {
	private static String MOBILE_VALIDATION = "^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[789]\\d{9}$";
	private static String EMPTY = "";

	public static Response validate(VerifyOtpRequest request) {
		Response response = new Response();
		response.setStatus(Status.FAILED);

		if (request == null) {
			response.setMessage("Invalid request body");
			return response;
		}
		StringBuilder sb = new StringBuilder();

		sb.append(validateMobile(request.getMobile()));
		String otp = request.getOtp() > 0 ? EMPTY : " | Invalid otp";
		sb.append(otp);

		if (sb.toString().isEmpty())
			response.setStatus(Status.OK);

		response.setMessage(sb.toString());
		return response;

	}

	public static Response validate(SendOtpRequest request) {
		String validateMobile = validateMobile(request.getMobile());
		Response res = new Response();
		res.setStatus(validateMobile.isEmpty() ? Status.OK : Status.FAILED);
		res.setMessage(validateMobile);
		return res;
	}

	private static String validateMobile(String mobile) {
		if (mobile == null || mobile.isEmpty()) {
			return "Mobile number is mandatory";
		} else {
			Pattern compile = Pattern.compile(MOBILE_VALIDATION);
			if (!mobile.startsWith("+91") || !compile.matcher(mobile).find()) {
				return "Invalid mobile number";
			}
		}
		return EMPTY;
	}

}
