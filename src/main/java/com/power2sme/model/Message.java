package com.power2sme.model;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document(collection = "message")
public class Message  extends AbstractEntity {
	private static final long serialVersionUID = 1L;

	private String email;
	private String emailFrom;
	private String ccEmail;
	private String message;
	private String subject;
	// when base 64 attached in mail body then file is required
	private boolean sent;
	private boolean error = false;


	public Message(String email, String subject, String message) {
		this.email = email;
		this.message = message;
		this.subject = subject;
	}
	private final String APPLICATION_KEY = "01401532-e652-464e-b250-eb68f585e5a4";

	
}
