package com.power2sme.model;

import java.io.File;
import java.util.List;

import javax.mail.BodyPart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;


@Component
public class EmailDispatcher implements Runnable {

	private static Logger logger = LoggerFactory.getLogger(EmailDispatcher.class);

	private boolean isRunning = true;

	private MongoTemplate mongoTemplate;

	private JavaMailSender mailSender;
	
	private String fromMail;

	public EmailDispatcher() {

	}

	public EmailDispatcher(MongoTemplate mongoTemplate, JavaMailSender mailSender, String fromMail) {
		this.mongoTemplate = mongoTemplate;
		this.mailSender = mailSender;
		this.fromMail = fromMail;
	}

	@Override
	public void run() {
		while (isRunning) {

			try {
				dispatchNewMails();

				Thread.sleep(1 * 30 * 1000);

			} catch (Exception e) {
				logger.info("Exception occured in EmailDispatcher :" + e.getMessage(), e);
			}

		}
	}

	private void dispatchNewMails() {
		Query query = new Query();
		query.addCriteria(Criteria.where("sent").is(false).and("error").is(false));
		List<Message> messages = mongoTemplate.find(query, Message.class);

		if (messages != null) {
			logger.info("New email message count : " + messages.size());
			for (Message message : messages) {
				try {
					
					sendPlainEmail(message);
				} catch (Exception e) {
					message.setError(true);
					mongoTemplate.save(message);
					logger.info("Error while sending mail :" + e.getMessage(), e);
				}
				message.setSent(true);
				mongoTemplate.save(message);
			}
		}
	}

	// required to scale when multiple attachment send at a time.
	// this method will send only pdf attachment

	public void sendPlainEmail(Message message) {
		logger.info("sending  plain email...");

		MimeMessagePreparator preparator = new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {

				mimeMessage.setRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(message.getEmail()));
				mimeMessage.setFrom(new InternetAddress(fromMail));

				MimeMultipart multipart = new MimeMultipart("related");

				BodyPart bodyPart = new MimeBodyPart();
				bodyPart.setContent(message.getMessage(), "text/html; charset=utf-8");
				multipart.addBodyPart(bodyPart);

				mimeMessage.setSubject(message.getSubject());
				mimeMessage.setContent(multipart);

			}
		};
		mailSender.send(preparator);

		logger.info("plain email sent.");

	}





	public void destroy() {
		logger.info("Closing email dispatcher service");
		isRunning = false;
		logger.info("Closed email dispatcher service");
	}

	public void deleteFile(File file) {
		boolean delete = file.delete();
		if (delete) {
			logger.info(file.getPath() + " is deleted!");
		} else {
			logger.info("unable to delete {}", file.getPath());
		}
	}
}
