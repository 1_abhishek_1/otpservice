package com.power2sme.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;


@Component
public class MessageProducer {
	private static Logger logger = LoggerFactory.getLogger(MessageProducer.class);

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired

	public MessageProducer() {

	}

	public void saveMail(String email, String subject, String body) {
		Message Message = new Message(email, subject, body);
		logger.info("Saving new email  message with attachments");
		mongoTemplate.save(Message);
		logger.info("Saving new email  message with attachments done.");
	}

	



	

	
}
