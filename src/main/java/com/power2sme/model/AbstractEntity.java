package com.power2sme.model;

import org.joda.time.DateTime;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Auditable;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

//@JsonIgnoreProperties({ "id", "new", "createdDate", "lastModifiedDate", "version" })

@Data
public  abstract class  AbstractEntity implements Auditable<String, String> {

	public AbstractEntity() {
        super();
    }
	private static final long serialVersionUID = -4997549280309658851L;

	@Id
	private String id;

	@Version
	private Long version;



	@CreatedDate
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy'T'HH:mm:ss.SSSZ", timezone = "IST")
	private DateTime createdDate;

	@LastModifiedDate
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy'T'HH:mm:ss.SSSZ", timezone = "IST")
	private DateTime lastModifiedDate;

	private String createdBy;

	private String lastModifiedBy;

	@Override
	public boolean isNew() {
		return id == null;
	}

	

}
