package com.power2sme.model;

import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.mapping.Document;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Document(collection = "otpDetails")
public class OtpDetails extends AbstractEntity {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	private int otp;
	private String mobile;
	private boolean isVerfied;
	private boolean isExpired;
	private int wrongAttempt;
	private DateTime verifiedTime;
	private DateTime sentTime;
	private DateTime otpGenerateTime;
	private String deviceId;
	private int geneCnt = 0;

}
