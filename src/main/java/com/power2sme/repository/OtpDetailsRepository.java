package com.power2sme.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.power2sme.model.OtpDetails;

public interface OtpDetailsRepository extends MongoRepository<OtpDetails, Long>{
	public OtpDetails findByMobile(String mobile);
	public OtpDetails findByMobileOrderByLastModifiedDateDesc(String mobile);
	public OtpDetails findByMobileOrderByCreatedDateDesc(String mobile);


	public OtpDetails findByMobileAndVerfied(String mobile, boolean verified);
	public OtpDetails findByMobileAndDeviceIdAndVerfied(String mobile,String deviceId, boolean verified);
	
}
