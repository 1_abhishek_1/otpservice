package com.power2sme;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Power2smeApplication {

	public static void main(String[] args) {
		SpringApplication.run(Power2smeApplication.class, args);
	}
}
