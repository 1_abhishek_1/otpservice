package com.power2sme.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.power2sme.dto.Response;
import com.power2sme.dto.SendOtpRequest;
import com.power2sme.dto.Status;
import com.power2sme.dto.VerifyOtpRequest;
import com.power2sme.service.OtpService;
import com.power2sme.validation.Validator;

@RestController
public class OtpController {
	@Autowired
	private OtpService otpService;

	@PostMapping("/sendOtp")
	public Response sendOtp(@RequestBody SendOtpRequest sendOtpRequest, HttpServletRequest request) {
		Response validate = Validator.validate(sendOtpRequest);
		if (validate.getStatus() == Status.FAILED)
			return validate;

		return otpService.sendOtp(sendOtpRequest);
	}

	@PostMapping("/verifyOtp")
	public Response verifyOtp(@RequestBody VerifyOtpRequest verifyOtpRequest, HttpServletRequest request) {
		Response validate = Validator.validate(verifyOtpRequest);
		if (validate.getStatus() == Status.FAILED)
			return validate;
		return otpService.verifyOtp(verifyOtpRequest);
	}

}
