package com.power2sme.service;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.power2sme.dto.SendOtpRequest;
import com.power2sme.dto.SendOtpResponse;
import com.power2sme.dto.Status;
import com.power2sme.dto.VerifyOtpRequest;
import com.power2sme.dto.VerifyOtpResponse;
import com.power2sme.model.OtpDetails;
import com.power2sme.repository.OtpDetailsRepository;
import com.power2sme.util.Util;

@Service
public class OtpService {
	private static final long FIFTEEN_MINUTES = 15 * 60 * 1000;
	private static final long BLOCKED_TIME = 24 *60 * 60 * 1000;
	private static Logger logger = LoggerFactory.getLogger(OtpService.class);

	@Autowired
	private OtpDetailsRepository otpDetailsRepository;

	public SendOtpResponse sendOtp(SendOtpRequest otpRequest) {
		SendOtpResponse otpResponse = new SendOtpResponse();

		OtpDetails otpDetail = otpDetailsRepository.findByMobileOrderByLastModifiedDateDesc(otpRequest.getMobile());

		if (isBlocked(otpDetail)) {
			otpResponse.setStatus(Status.OK);
			// static method.. we can sent back time as well to retry
			otpResponse.setMessage("Your number have been blocked ..try again tomorrow.");
		} else {
			// when otp is expired or already verified and wanted to generate new otp
			if (isExpired(otpDetail) || otpDetail.isVerfied()) {
				int otp = Util.randome();
				otpDetail = OtpDetails.builder().otp(otp).mobile(otpRequest.getMobile())
						.deviceId(otpRequest.getDeviceId()).otpGenerateTime(DateTime.now()).build();
				otpDetail.setCreatedDate(DateTime.now());
				otpDetail.setLastModifiedDate(DateTime.now());
			} else {
				otpDetail.setGeneCnt(otpDetail.getGeneCnt() + 1);
				otpDetail.setLastModifiedDate(DateTime.now());
			}
			otpDetailsRepository.save(otpDetail);
			logger.info("Otp " + otpDetail.getOtp());
			
			// uncomment this line of code to actually send otp  due to chareges may apply
			// Util.sendMessage("Otp : " + otpDetail.getOtp());
			otpResponse.setStatus(Status.OK);
			otpResponse.setMessage("Successfully otp have been sent");
		}
		return otpResponse;
	}

	public VerifyOtpResponse verifyOtp(VerifyOtpRequest otpRequest) {
		VerifyOtpResponse otpResponse = new VerifyOtpResponse();

		OtpDetails otpDetail = otpDetailsRepository.findByMobileOrderByLastModifiedDateDesc(otpRequest.getMobile());
		
		// if any of the condition hold true ,   is a invalid request
		if (isExpired(otpDetail) || isBlocked(otpDetail)|| otpDetail.getWrongAttempt() >= 3) {
			otpResponse.setStatus(Status.FAILED);
			otpResponse.setMessage("Invalid verfication request");
		} else {
			if (otpDetail.isVerfied() == true) {
				otpResponse.setMessage("Mobile already verified");
				otpResponse.setStatus(Status.OK);
			} else if (otpDetail.getOtp() == otpRequest.getOtp()) {
				otpDetail.setVerfied(true);
				otpResponse.setMessage("Mobile number verified");
				otpResponse.setStatus(Status.OK);
			} else {
				otpDetail.setWrongAttempt(otpDetail.getWrongAttempt() + 1);
				otpResponse.setMessage("Could not able to verify otp");
				otpResponse.setStatus(Status.FAILED);
			}
			otpDetailsRepository.save(otpDetail);
		}
		return otpResponse;
	}

	private boolean isBlocked(OtpDetails otpDetail) {
		if (otpDetail != null && otpDetail.getGeneCnt() >= 3) {
			long lastupdate = otpDetail.getLastModifiedDate().getMillis();
			long now = DateTime.now().getMillis();
			if (now - lastupdate <= BLOCKED_TIME) {
				return true;
			}
		}
		return false;
	}

	private boolean isExpired(OtpDetails otpDetail) {
		if (otpDetail == null)
			return true;
		long otpGenerateTime = otpDetail.getOtpGenerateTime().getMillis();
		long now = DateTime.now().getMillis();
		if (now - otpGenerateTime > FIFTEEN_MINUTES)
			return true;
		return false;
	}
}
