package com.power2sme.dto;

import lombok.Data;

@Data
public class Response {
	private Status status;
	private String message;
}
