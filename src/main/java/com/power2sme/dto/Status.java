package com.power2sme.dto;

public enum Status {
	OK("Ok"), FAILED("Failure");

	private String status;

	Status(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}
}
