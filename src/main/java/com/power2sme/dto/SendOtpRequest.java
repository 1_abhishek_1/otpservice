package com.power2sme.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import lombok.Data;

@Data
public class SendOtpRequest {
	@NotNull 
	@Length(min=10, max = 13)
	private String mobile;
	private String deviceId;
}
