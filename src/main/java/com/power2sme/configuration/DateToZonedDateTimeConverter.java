package com.power2sme.configuration;

import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.core.convert.converter.Converter;

public class DateToZonedDateTimeConverter implements Converter<DateTime, Date> {

	public static final DateToZonedDateTimeConverter INSTANCE = new DateToZonedDateTimeConverter();

	public DateToZonedDateTimeConverter() {
	}

	@Override
	public Date convert(DateTime source) {
		return source == null ? null : source.toDate();
	}
}
