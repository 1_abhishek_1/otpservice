package com.power2sme.configuration;

import java.util.Date;

import org.joda.time.DateTime;
import org.springframework.core.convert.converter.Converter;

public class ZonedDateTimeToDateConverter implements Converter<Date, DateTime> {

	public static final ZonedDateTimeToDateConverter INSTANCE = new ZonedDateTimeToDateConverter();

	public ZonedDateTimeToDateConverter() {
	}

	@Override
	public DateTime convert(Date source) {
		return source == null ? null : new DateTime(source);
	}
}
